# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

##[1.17.1] - 2021-06-28
### Chore
- bumped to nodevv v1.17.1
- all plugins bumped to latest version
### Fixed
- fixed nsawards plugin
### Add
- added discord notifications plugin

## [1.14.3] - 2020-09-13
### Chore
- bumped to nodebb v1.14.3
- bumped to node 14.10

## [1.13.2] - 2020-04-01
### Chore
- bumped plugins to latest version

## [1.13.2] - 2020-03-31
### Chore
- bumped to nodebb v1.13.2

## [1.5.2-1] - 2016-07-16
### Fixed
- fixed host2ips.sh not found

## [1.5.2] - 2016-07-16
### Changed
- bumped to nodebb 1.5.2

### Fixed
- fixed error when starting image caused by colors module

## [1.5.1] - 2016-07-02
### Added
- bumped to nodebb 1.5.1
- bumped to nodejs 8.1.2 image
- added MONGO_EXPAND env variable

## [1.4.6] - 2017-07-01
### Changed
- bumped to nodebb 1.4.6
